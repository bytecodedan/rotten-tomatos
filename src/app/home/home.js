angular.module('app.home', [])

.config(['$routeProvider', function ($routeProvider) {
  $routeProvider.when('/', {
    templateUrl:'home/home.tpl.html'
  });
}]);