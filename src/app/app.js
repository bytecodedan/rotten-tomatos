var App = angular.module('movieRatingApp', [
	'ngRoute', 
	'app.home', 
	'app.ratings',
	'templates.app'])

.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'home/home.tpl.html'
    })
    .otherwise({
      templateUrl: '404.tpl.html'
    });
}])

.controller('AppCtrl', ['$scope', function($scope) {
  $scope.user = {
    name: 'Dan Martin',
    email: 'mailto:dlmartin81@gmail.com',
    linkedIn: 'http://lnkd.in/W5ta4U',
    twitter: 'https://twitter.com/dlmartin81',
    gmail: 'https://plus.google.com/u/0/101468066663760476923/posts/p/pub'
  };
  $scope.navItems = [
    {name: "Home", url: "#/"}, 
    {name: "Ratings", url: "#/ratings"}
  ];
  
  $scope.selectedIndex = 0;
  
  $scope.itemClicked = function ($index) {
    $scope.selectedIndex = $index;
  };
}]);