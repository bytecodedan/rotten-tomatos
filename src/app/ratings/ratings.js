angular.module('app.ratings', ['ui.bootstrap'])

.config(['$routeProvider', '$httpProvider', function ($routeProvider) {
  $routeProvider.when('/ratings', {
    templateUrl:'ratings/ratings.tpl.html'
  });
}])

.controller('RatingsGridCtrl', ['$scope', function($scope) {
	// Static collection.
	var data = [
	  { "id": 1, "title": "Dark Knight", "rating": 87.4, "gross": 28083432 },
	  { "id": 2, "title": "Ironman", "rating": 82.1, "gross": 23443425 },
	  { "id": 3, "title": "Spiderman", "rating": 85.2, "gross": 59234252 },
	  { "id": 4, "title": "Mr. Been", "rating": 78.6, "gross": 93812755 },
	  { "id": 5, "title": "Blues Brothers", "rating": 90.5, "gross": 98762325 },
	  { "id": 6, "title": "Shrek", "rating": 89.2, "gross": 8342363 },
	  { "id": 8, "title": "Black Hawk Down", "rating": 73.8, "gross": 44202341 },
	  { "id": 9, "title": "The Good Shepard", "rating": 85.3, "gross": 2342516 },
	  { "id": 10, "title": "Dukes of Hazard", "rating": 67.4, "gross": 2303463 },
	  { "id": 11, "title": "Ironman 2", "rating": 84.3, "gross": 69161831 },
	  { "id": 12, "title": "Top Gun", "rating": 88.6, "gross": 498723511 },
	  { "id": 13, "title": "Jarhead", "rating": 67.8, "gross": 123145 },
	  { "id": 14, "title": "Independence Day", "rating": 87.8, "gross": 4292872454 },
	  { "id": 15, "title": "The Incredible Hulk", "rating": 77.4, "gross": 9563254 },
	  { "id": 16, "title": "Man of Fire", "rating": 79.9, "gross": 6328362 },
	  { "id": 17, "title": "The Recruit", "rating": 76.9, "gross": 8661725 },
	  { "id": 18, "title": "Robin Hood, Man in Tights", "rating": 56.2, "gross": 243637 },
	  { "id": 19, "title": "Barney and Friends", "rating": 46.3, "gross": 98272 },
	  { "id": 20, "title": "Fast and the Furious", "rating": 84.4, "gross": 4928372622 },
	  { "id": 21, "title": "Ironman 3", "rating": 83.2, "gross": 927262423 },
	  { "id": 22, "title": "Finding Nemo", "rating": 92.8, "gross": 44 },
	  { "id": 23, "title": "Deep Blue Sea", "rating": 34.5, "gross": 87723 },
	  { "id": 24, "title": "Ice Age", "rating": 89.7, "gross": 92323666 },
	  { "id": 25, "title": "Ice Age 2", "rating": 94.3, "gross": 491392396 },
	  { "id": 26, "title": "Mary Poppins", "rating": 66.2, "gross": 2982342 },
	  { "id": 27, "title": "Groundhog Day", "rating": 86.2, "gross": 928349209 },
	  { "id": 28, "title": "Ghost Busters", "rating": 72.7, "gross": 23124513 },
	  { "id": 29, "title": "Bewitched", "rating": 67.2, "gross": 1213156 },
	  { "id": 30, "title": "Borat", "rating": 47.2, "gross": 1214673 },
	  { "id": 31, "title": "Talladega Nights", "rating": 56.2, "gross": 234660 },
	  { "id": 32, "title": "Boogie Nights", "rating": 66.2, "gross": 3234660 }
	];

	// The paged data (subset of data)
	$scope.movies = [];

	var compareTitle = function(a, b) {
	  if (a.title < b.title) { return -1; }
	  if (a.title > b.title) { return 1; }
	  return 0;
	};

	var compareRating = function(a, b) {
	  if (a.rating < b.rating) { return -1; }
	  if (a.rating > b.rating) { return 1; }
	  return 0;
	};

	var compareGross = function(a, b) {
	  if (a.gross < b.gross) { return -1; }
	  if (a.gross > b.gross) { return 1; }
	  return 0;
	};

	var setPage = function(newPage) {
		// Subtract one from parameter 'newPage' because
		// JS arrays are zero-based.
		var start = (newPage - 1) * $scope.pageSize;
		var end = start + $scope.pageSize;
		$scope.movies = data.slice(start, end);
	};

	// Some constants.
	var sortOrderASC = 'asc';
	var sortOrderDESC = 'desc';
	var colTitle = 'title';
	var colRating = 'rating';
	var colGross = 'gross';

	// Table default settings.
	var defaults = {
		totalItems: data.length,
		currentPage: 1,
  	pageSize: 5,
  	numOfPages: 3,
  	maxSize: 5,
  	sortOrder: sortOrderASC,
  	sortColumn: colRating
	};

	// Table current settings.
	$scope.totalItems = defaults.totalItems;
  $scope.currentPage = defaults.currentPage;
  $scope.pageSize = defaults.pageSize;
  $scope.numOfPages = defaults.numOfPages;
  $scope.maxSize = defaults.maxSize;
  $scope.sortOrder = defaults.sortOrder;
  $scope.sortColumn = defaults.sortColumn;

  $scope.sort = function(col) {
  	// Determine column to sort by.
  	if (col === 'title') {
  		data = data.sort(compareTitle);
  	} else if (col === 'rating') {
  		data = data.sort(compareRating);
  	} else if (col === 'gross') {
  		data = data.sort(compareGross);
  	}

  	// Determine ordering.
  	if ($scope.sortOrder === sortOrderASC) {
  		// If sorting on same column, reverse the order.
  		if (col === $scope.sortColumn) {
  			$scope.sortOrder = sortOrderDESC;
  			data = data.reverse();
  		} else {
  			$scope.sortColumn = col;
  		}
  	} else {
  		if (col === $scope.sortColumn) {
  			$scope.sortOrder = sortOrderASC;
  		} else {
  			$scope.sortColumn = col;
  		}
  	}

  	$scope.currentPage = defaults.currentPage;
   	setPage(1);
  };

  $scope.$watch('currentPage', function(newPage){
    $scope.watchPage = newPage;
		setPage(newPage);
  });

  // Set default sort
  $scope.sort($scope.sortColumn);
}]);